jQuery("#simulation")
    .on("click", ".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .click", function (event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) {
            data = event;
        }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Label_24")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimMove",
                                    "parameter": {
                                        "target": ["#s-Line_9"],
                                        "top": {
                                            "type": "movetoposition",
                                            "value": "46"
                                        },
                                        "left": {
                                            "type": "movetoposition",
                                            "value": "0"
                                        },
                                        "containment": false,
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Label_25")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimMove",
                                    "parameter": {
                                        "target": ["#s-Line_9"],
                                        "top": {
                                            "type": "movetoposition",
                                            "value": "47"
                                        },
                                        "left": {
                                            "type": "movetoposition",
                                            "value": "105"
                                        },
                                        "containment": false,
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Label_33")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimMove",
                                    "parameter": {
                                        "target": ["#s-Line_9"],
                                        "top": {
                                            "type": "movetoposition",
                                            "value": "47"
                                        },
                                        "left": {
                                            "type": "movetoposition",
                                            "value": "210"
                                        },
                                        "containment": false,
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimScrollTo",
                                    "parameter": {
                                        "target": ["#s-Label_24"],
                                        "axis": "scrollx",
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Label_34")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimMove",
                                    "parameter": {
                                        "target": ["#s-Line_9"],
                                        "top": {
                                            "type": "movetoposition",
                                            "value": "47"
                                        },
                                        "left": {
                                            "type": "movetoposition",
                                            "value": "315"
                                        },
                                        "containment": false,
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimScrollTo",
                                    "parameter": {
                                        "target": ["#s-Label_33"],
                                        "axis": "scrollx",
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        } else if (jFirer.is("#s-Label_35")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_25 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimMove",
                                    "parameter": {
                                        "target": ["#s-Line_9"],
                                        "top": {
                                            "type": "movetoposition",
                                            "value": "47"
                                        },
                                        "left": {
                                            "type": "movetoposition",
                                            "value": "420"
                                        },
                                        "containment": false,
                                        "effect": {
                                            "type": "none",
                                            "easing": "linear",
                                            "duration": 200
                                        }
                                    },
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_34 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_33 span": {
                                            "attributes": {
                                                "color": "#898989",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                },
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_35 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        }
    })
    .on("pageload", ".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .pageload", function (event, data) {
        var jEvent, jFirer, cases;
        if (data === undefined) {
            data = event;
        }
        jEvent = jimEvent(event);
        jFirer = jEvent.getEventFirer();
        if (jFirer.is("#s-Label_24")) {
            cases = [
                {
                    "blocks": [
                        {
                            "actions": [
                                {
                                    "action": "jimChangeStyle",
                                    "parameter": [{
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24": {
                                            "attributes": {
                                                "font-size": "10.0pt",
                                                "font-family": "'Roboto-Regular',Arial"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 .valign": {
                                            "attributes": {
                                                "vertical-align": "middle",
                                                "text-align": "center"
                                            }
                                        }
                                    }, {
                                        "#s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Label_24 span": {
                                            "attributes": {
                                                "color": "#21C0C0",
                                                "text-align": "center",
                                                "text-decoration": "none",
                                                "font-family": "'Roboto-Regular',Arial",
                                                "font-size": "10.0pt"
                                            }
                                        }
                                    }],
                                    "exectype": "serial",
                                    "delay": 0
                                }
                            ]
                        }
                    ],
                    "exectype": "serial",
                    "delay": 0
                }
            ];
            event.data = data;
            jEvent.launchCases(cases);
        }
    });