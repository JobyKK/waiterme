(function (window, undefined) {

    /*********************** START STATIC ACCESS METHODS ************************/

    jQuery.extend(jimMobile, {
        "loadScrollBars": function () {
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .ui-page").overscroll({
                showThumbs: true,
                direction: 'vertical'
            });
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Panel_12").overscroll({
                showThumbs: false,
                direction: 'horizontal'
            });
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 #s-Cell_center .layout").overscroll({
                showThumbs: true,
                direction: 'vertical'
            });
        }
    });

    /*********************** END STATIC ACCESS METHODS ************************/

})(window);