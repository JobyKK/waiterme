# coding=utf-8

import os


DEBUG = os.environ.get('DEBUG', True)
DB_URL = (
    'postgres://{env[PG_USER]}:{env[PG_PASSWORD]}'
    '@{env[PG_HOST]}:{env[PG_PORT]}/{env[PG_DB]}'
).format(env=os.environ)
