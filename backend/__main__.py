# coding=utf-8

import asyncio
import json
import logging

import sys
from aiohttp import web, WSMsgType
from aiopg.sa import create_engine

from backend import DB_URL, DEBUG
from backend import routes
import aiohttp_cors

from backend.lib.utils.ws import prepare_message
from backend.services import WS_ROUTES

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('asyncio').setLevel(logging.WARNING)

log = logging.getLogger(__name__)


async def db_shutdown(app):
    engine = app['engine']
    engine.close()


async def init_db(app):
    app['engine'] = await create_engine(DB_URL)
    app.on_shutdown.append(db_shutdown)
    return app


async def middleware(app, handler):
    async def middleware_handler(request):
        log.info(request.path)
        try:
            response = await handler(request)
        except web.HTTPException as ex:
            if ex.status == 404:
                response = await mock_controller(request)
            else:
                raise

        return response
    return middleware_handler

async def mock_controller(request):
    data = request.GET if request.GET else await request.post()
    data = {
        'url': request.path,
        'data': dict(data),
        'status': 'success'
    }
    return web.Response(body=json.dumps(data).encode(), content_type='application/json')

async def websocket_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type == WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                data = msg.data and json.loads(msg.data)
                if data and data['code']:
                    await asyncio.sleep(1)
                    try:
                        await WS_ROUTES[data['code']](data['data'], ws)
                    except Exception as e:
                        type_, value_, traceback_ = sys.exc_info()
                        await WS_ROUTES['ERROR']({
                            'type': type_,
                            'value': value_,
                            'traceback': traceback_
                        }, ws)

                else:
                    await ws.send_json(prepare_message('ANSWER', {
                        'data': msg.data
                    }))
        elif msg.type == WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    print('websocket connection closed')

    return ws

async def create_app(loop):
    app = web.Application(loop=loop)
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    app.middlewares.append(middleware)
    routes.setup(app)
    app.router.add_route('GET', '/ws', websocket_handler, name='WS')
    return app

main_loop = asyncio.get_event_loop()
main_loop.set_debug(DEBUG)
main_app = main_loop.run_until_complete(create_app(main_loop))
# main_app = main_loop.run_until_complete(init_db(main_app))
