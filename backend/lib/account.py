# coding=utf-8

import aiohttp
import logging
from json.decoder import JSONDecodeError

from novaposhta import DEBUG, TEST_API_KEY
from novaposhta.lib.query.account import get_company_account
from novaposhta.model import Account


log = logging.getLogger(__name__)


async def load_company_info(company_id):
    if DEBUG:
        return {
            'company_id': company_id,
            'api_key': TEST_API_KEY,
        }
    return {}


async def load_account(engine, company_id):
    company_data = await load_company_info(company_id)
    log.debug('[fetch_account] company_data %s', company_data)
    if company_data:
        async with engine.acquire() as conn:
            await conn.execute(Account.insert().values(
                company_id=company_id,
                api_key=company_data['api_key'],
            ))
    return company_data


def fetch_account(handler, *args, **kwargs):
    async def wrapper(cls, request):
        engine = request.app['engine']
        kwargs['account'] = None
        try:
            kwargs['body'] = await request.json()
        except JSONDecodeError:
            kwargs['body'] = {}
        company_id = kwargs['body'].get('company_id')
        if company_id and isinstance(company_id, int):
            account = await get_company_account(engine, company_id)
            if not account:
                company_data = await load_account(engine, company_id)
                account = await get_company_account(engine, company_data['company_id'])
            kwargs['account'] = account
        return await handler(cls, request, *args, **kwargs)
    return wrapper
