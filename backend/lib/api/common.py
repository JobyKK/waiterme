import logging

import aiohttp
import simplejson

from novaposhta import NOVA_POSHTA_API_URL

log = logging.getLogger(__name__)


async def make_api_request(api_key, model, method, props):
    log.debug('[make_api_request] %s %s %s %s', api_key, model, method, props)

    async with aiohttp.ClientSession() as client:
        async with client.post(
            NOVA_POSHTA_API_URL,
            data=simplejson.dumps({
                'apiKey': api_key,
                'modelName': model,
                'calledMethod': method,
                'methodProperties': props,
            })
        ) as resp:
            assert resp.status == 200
            resp_json = await resp.json()
            log.debug('[make_api_request] body: %s', resp_json)
            return resp_json
