from novaposhta.lib.api.common import make_api_request

COUNTERPARTY_SENDER = 'Sender'
COUNTERPARTY_RECIPIENT = 'Recipient'


async def get_counterparties(
    api_key,
    counterparty_type=COUNTERPARTY_SENDER,
    page='1',
):
    response = await make_api_request(
        api_key=api_key,
        model='Counterparty',
        method='getCounterparties',
        props={
            'CounterpartyProperty': counterparty_type,
            'Page': page
        }
    )
    if response and 'data' in response:
        return response['data']
