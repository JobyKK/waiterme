from novaposhta.lib.api.common import make_api_request


async def get_tracking_document_list(
        api_key,
        internet_document_refs,
):
    response = await make_api_request(
        api_key=api_key,
        model='TrackingDocument',
        method='getStatusDocuments',
        props={
            'Documents': internet_document_refs,
            'Language': 'UA',
        }
    )
    if response and 'data' in response:
        return response['data']


async def get_internet_document_list(
        api_key,
        get_full_list='',
        date_time='',
):
    response = await make_api_request(
        api_key=api_key,
        model='InternetDocument',
        method='getDocumentList',
        props={
            'GetFullList': get_full_list,
            'DateTime': date_time,
        }
    )
    if response and 'data' in response:
        return response['data']
