# coding=utf-8
from novaposhta.lib.utils.common import result_to_dict
from novaposhta.lib.query.internet_document import get_internet_document_list_query
from novaposhta.lib.pagination import Page


async def internet_document_list_context(engine, account_id, items_per_page, page_num):
    query = get_internet_document_list_query(account_id)
    page = await Page(engine, query, items_per_page, page_num).init()
    result = await page.page_items()
    return {
        'documents': [result_to_dict(document) for document in result],
        'pagination': {
            'current_page': page.current_page,
            'last_page': page.last_page,
            'num_pages': page.page_count,
            'items_per_page': page.items_per_page,
        }
    }
