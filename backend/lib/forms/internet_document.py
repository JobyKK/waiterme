# coding=utf-8
from datetime import datetime

from wtforms import Form
from wtforms.fields import BooleanField
from wtforms.fields import DateField, DateTimeField
from wtforms.fields import DecimalField
from wtforms.fields import FloatField
from wtforms.fields import FormField, FieldList
from wtforms.fields import IntegerField
from wtforms.fields import StringField
from wtforms.validators import Length, NumberRange

MAX_INTEGER_VALUE = 2**31-1


class RecipientAddressForm(Form):
    address_ref = StringField(validators=[Length(max=255)])
    street_ref = StringField(validators=[Length(max=255)])
    street_name = StringField(validators=[Length(max=255)])
    street_type = StringField(validators=[Length(max=255)])
    building_num = StringField(validators=[Length(max=255)])
    flat = StringField(validators=[Length(max=255)])
    note = StringField(validators=[Length(max=255)])


class SeatsOptionForm(Form):
    weight = FloatField(validators=[NumberRange(min=0.1, max=10000)])
    volumetric_height = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    volumetric_width = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    volumetric_length = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])


class BackwardDeliveryData(Form):
    cargo_type = StringField(validators=[Length(max=255)])
    payer_type = StringField(validators=[Length(max=255)])
    redelivery_string = StringField(validators=[Length(max=255)])
    redelivery_amount = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])


class InternetDocumentParamsForm(Form):
    backward_delivery_data = FormField(form_class=BackwardDeliveryData)
    seats_option = FieldList(FormField(form_class=SeatsOptionForm), min_entries=0, max_entries=20)
    recipient_address = FormField(form_class=RecipientAddressForm)


class InternetDocumentForm(Form):
    id = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    ref = StringField(validators=[Length(max=255)])

    order_id = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    account_id = IntegerField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    date_modified = DateTimeField(default=datetime.now)

    payer_type = StringField(validators=[Length(max=255)])
    payment_method = StringField(validators=[Length(max=255)])
    send_date = DateField(default=datetime.now)
    cargo_type = StringField(validators=[Length(max=255)])
    volume_general = StringField(validators=[Length(max=255)])
    weight = StringField(validators=[Length(max=255)])
    service_type = StringField(validators=[Length(max=255)])
    seats_amount = StringField(validators=[Length(max=255)])
    description = StringField(validators=[Length(max=1000)])
    cost = DecimalField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    city_sender = StringField(validators=[Length(max=255)])
    sender = StringField(validators=[Length(max=255)])
    sender_address = StringField(validators=[Length(max=255)])
    contact_sender = StringField(validators=[Length(max=255)])
    senders_phone = StringField(validators=[Length(max=255)])
    city_recipient = StringField(validators=[Length(max=255)])
    recipient = StringField(validators=[Length(max=255)])
    recipient_address = StringField(validators=[Length(max=255)])
    contact_recipient = StringField(validators=[Length(max=255)])
    recipients_phone = StringField(validators=[Length(max=255)])
    packing_number = StringField(validators=[Length(max=255)])
    cost_on_site = DecimalField(validators=[NumberRange(min=1, max=MAX_INTEGER_VALUE)])
    estimated_delivery_date = StringField(validators=[Length(max=10)])
    int_doc_number = StringField(validators=[Length(max=255)])
    type_document = StringField(validators=[Length(max=255)])
    region_code = StringField(validators=[Length(max=255)])
    region_city = StringField(validators=[Length(max=255)])
    printed = BooleanField(default=False)
    params = FormField(form_class=InternetDocumentParamsForm)
