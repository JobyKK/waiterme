import json
import datetime

from aiohttp import web
import simplejson


class CustomJSONEncoder(simplejson.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return obj.isoformat()  # obj.strftime("%d.%m.%Y")
        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()
        else:
            return super(CustomJSONEncoder, self).default(obj)


def to_json(handler, *args, **kwargs):
    async def wrapper(cls, request):
        dict_data = await handler(cls, request)
        return json_response(dict_data, *args, **kwargs)
    return wrapper


def json_response(data, *args, **kwargs):
    kwargs['content_type'] = kwargs.get('content_type', 'application/json')
    kwargs['text'] = simplejson.dumps(data, cls=CustomJSONEncoder)
    return web.Response(*args, **kwargs)
