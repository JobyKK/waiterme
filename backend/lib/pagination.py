# coding=utf-8


class Page:

    def __init__(self, engine, query, items_per_page, page=1, *args, **kwargs):
        super(Page, self).__init__(*args, **kwargs)

        self.engine = engine
        self.query = query
        self.items_per_page = items_per_page
        self.items_count = 0
        self.page_count = 1
        self.first_page = 1
        self.last_page = 1
        self.current_page = page

    async def init(self):
        self.items_count = await self.get_items_count()
        self.page_count = int((self.items_count - 1) / self.items_per_page) + 1

        self.last_page = self.first_page + self.page_count - 1

        if self.current_page > self.last_page:
            self.current_page = self.last_page
        elif self.current_page < self.first_page:
            self.current_page = self.first_page

        return self

    async def get_items_count(self):
        async with self.engine.acquire() as conn:
            result = await conn.execute(self.query.alias('foo').count())
            return await result.scalar()

    async def page_items(self):
        async with self.engine.acquire() as conn:
            result = await conn.execute((
                self.query
                .offset(self.items_per_page * (self.current_page - 1))
                .limit(self.items_per_page)
            ))
            return await result.fetchall()
