# coding=utf-8
from novaposhta.model import Account
from novaposhta.lib.query.common import execute_query


async def get_company_account(engine, company_id):
    query = await execute_query(
        engine,
        Account.select()
        .where(Account.c.company_id == company_id)
    )
    return await query.first()
