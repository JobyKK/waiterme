
async def execute_query(engine, query):
    async with engine.acquire() as conn:
        return await conn.execute(query)
