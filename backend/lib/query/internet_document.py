# coding=utf-8
from sqlalchemy.sql.expression import select, join

from novaposhta.model import InternetDocument
from novaposhta.model import ContactPerson
from novaposhta.lib.query.common import execute_query


def get_internet_document_list_query(account_id):
    return (
        select([
            InternetDocument,
            ContactPerson.c.first_name,
            ContactPerson.c.last_name
        ])
        .select_from(InternetDocument.outerjoin(
            ContactPerson,
            InternetDocument.c.contact_recipient == ContactPerson.c.ref
        ))
        .where(InternetDocument.c.account_id == account_id)
    )


async def get_internet_document_by_id(engine, internet_document_id):
    async with engine.acquire() as conn:
        query = await conn.execute(
            InternetDocument.select()
            .where(InternetDocument.c.id == internet_document_id)
        )
        return await query.first()


async def create_internet_document(engine, internet_document_props):
    async with engine.acquire() as conn:
        query = await conn.execute(
            InternetDocument.insert(),
            internet_document_props,
        )
        return await query.first()
