from aiohttp.web import Response

from backend.services import WS_ROUTES


def post(fn):
    async def func(self, request):
        if request.method != 'POST':
            return Response(status=405)
        request.data = request.POST
        return await fn(self, request)
    return func


def get(fn):
    async def func(self, request):
        if request.method != 'GET':
            return Response(status=405)
        request.data = request.GET
        return await fn(self, request)
    return func


def ws(route=None):
    def wrap_func(fn):
        WS_ROUTES[route if route else fn.__name__.upper()] = fn
        print(WS_ROUTES)
        async def func(self, request):
            return await fn(self, request)
        return func
    return wrap_func
