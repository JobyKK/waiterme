from aiohttp.hdrs import METH_ALL, METH_ANY


class Route(object):

    method = None
    path = None
    handler = None
    name = None

    def __init__(self, method, path, handler, name=''):
        if method in METH_ALL or method == METH_ANY:
            self.method = method
        else:
            raise Exception('''method must be in ('POST', 'GET', 'OPTIONS', 'PUT', 'DELETE')''')
        assert type(path) == str
        self.path = path
        if callable(handler):
            self.handler = handler
        else:
            raise Exception('handler bust be is function')
        assert type(path) == str
        self.name = name

    def repr(self, add_path=''):
        path = '' if add_path.startswith('/') else '/' + add_path + self.path
        return self.method, path, self.handler

