def prepare_message(code, data):
    return {
        'code': code,
        'data': data
    }
