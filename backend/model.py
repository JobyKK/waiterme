# coding=utf-8
from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSON


metadata = sa.MetaData()


Account = sa.Table(
    'account',
    metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('company_id', sa.Integer),
    sa.Column('api_key', sa.String(255)),
)
