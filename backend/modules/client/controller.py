import logging
from traceback import format_tb

from aiohttp.web import Response

from backend.lib.utils.controller import BaseController
from backend.lib.utils.decorators import get, ws
from backend.lib.utils.ws import prepare_message

log = logging.getLogger(__name__)


class ClientController(BaseController):

    @get
    async def index(self, request):
        return Response(body='hello world'.encode(), content_type='text/html')

    @staticmethod
    @ws()
    async def send_order(data, web_socket):
        return await web_socket.send_json(prepare_message(
            'SEND_ORDER_SUCCESS',
            {
                'order_id': 2
            }))

    @staticmethod
    @ws()
    async def error(err, web_socket):
        tb = format_tb(err['traceback'])
        tb.insert(0, str(err['type']) + ': ' + str(err['value']) + '\n')
        format_err = ''.join(tb)
        print(format_err)
        return await web_socket.send_json(
            prepare_message('ERROR', {
                'error': format_err
            }))
