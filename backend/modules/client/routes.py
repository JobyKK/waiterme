from backend.lib.utils.routes import Route
from backend.modules.client.controller import ClientController


client = ClientController()

routes = [
    Route('GET', '/', client.index, name='index')
]
