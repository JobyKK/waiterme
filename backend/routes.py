from backend.modules.client.routes import routes as client_routes


def setup(app):
    url = app.router.add_route
    for r in client_routes:
        url(*r.repr('client'), name='client_' + r.name)
