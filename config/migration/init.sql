CREATE TABLE account (
  id SERIAL NOT NULL,
  company_id INTEGER,
  api_key CHARACTER VARYING(255),
  CONSTRAINT account_pkey PRIMARY KEY (id),
  CONSTRAINT company_id_unique UNIQUE (company_id)
);


CREATE TABLE counterparty (
  id SERIAL NOT NULL,
  ref CHARACTER VARYING(255),
  account_id INTEGER,
  description CHARACTER VARYING(1000),
  property CHARACTER VARYING(255),
  type CHARACTER VARYING(255),
  CONSTRAINT counterparty_pk PRIMARY KEY (id),
  CONSTRAINT counterparty_ref_unique UNIQUE (ref),
  CONSTRAINT counterparty_account_id_fk FOREIGN KEY (account_id)
    REFERENCES account (id)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE contact_person (
  id SERIAL NOT NULL,
  ref CHARACTER VARYING(255),
  account_id INTEGER,
  counterparty_ref CHARACTER VARYING(255),
  description CHARACTER VARYING(1000),
  phone CHARACTER VARYING(255),
  email CHARACTER VARYING(255),
  last_name CHARACTER VARYING(255),
  first_name CHARACTER VARYING(255),
  middle_name CHARACTER VARYING(255),
  data JSON,
  CONSTRAINT contact_person_pk PRIMARY KEY (id),
  CONSTRAINT contact_person_ref_unique UNIQUE (ref),
  CONSTRAINT contact_person_account_id_fk FOREIGN KEY (account_id)
    REFERENCES account (id)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE internet_document (
  id SERIAL NOT NULL,
  order_id INTEGER,
  account_id INTEGER NOT NULL,
  date_modified TIMESTAMP WITHOUT TIME ZONE,
  ref CHARACTER VARYING(255),
  payer_type CHARACTER VARYING(255),
  payment_method CHARACTER VARYING(255),
  send_date DATE,
  cargo_type CHARACTER VARYING(255),
  volume_general CHARACTER VARYING(255),
  weight CHARACTER VARYING(255),
  service_type CHARACTER VARYING(255),
  seats_amount CHARACTER VARYING(255),
  description CHARACTER VARYING(1000),
  cost NUMERIC (16, 2),
  city_sender CHARACTER VARYING(255),
  sender CHARACTER VARYING(255),
  sender_address CHARACTER VARYING(255),
  contact_sender CHARACTER VARYING(255),
  senders_phone CHARACTER VARYING(255),
  city_recipient CHARACTER VARYING(255),
  recipient CHARACTER VARYING(255),
  recipient_address CHARACTER VARYING(255),
  contact_recipient CHARACTER VARYING(255),
  recipients_phone CHARACTER VARYING(255),
  packing_number CHARACTER VARYING(255),
  printed BOOLEAN,
  cost_on_site NUMERIC (16, 2),
  estimated_delivery_date DATE,
  int_doc_number CHARACTER VARYING(255),
  type_document CHARACTER VARYING(255),
  region_code CHARACTER VARYING(255),
  region_city CHARACTER VARYING(255),
  params JSON,
  CONSTRAINT internet_document_pk PRIMARY KEY (id),
  CONSTRAINT internet_document_ref_unique UNIQUE (ref),
  CONSTRAINT internet_document_account_id_fk FOREIGN KEY (account_id)
    REFERENCES account (id)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);
