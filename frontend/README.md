## Isomorphic Redux Demo

Basic isomorphic app built on [Redux](https://github.com/gaearon/redux)

```
$ npm i
$ npm run dev
$ npm start
$ browser http://localhost:3000

```
