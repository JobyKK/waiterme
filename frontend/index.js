require('babel-core/register')({});
require('babel-polyfill');

const server = require('./server').default;

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  // es-lint-disable-next-line
  console.log(`Server listening on: ${PORT}`);
});
