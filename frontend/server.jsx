var reduxRouter = require('redux-router');
var reduxServer = require('redux-router/server');
var reactServer = require('react-dom/server');
var createLocation = require('history/lib/createLocation');
require('babel-polyfill');
var React = require('react');
var reactRedux = require('react-redux');
var configureStore = require('./src/model/index');
var routes = require('routes');
var path = require('path');
var fetchComponentData = require('lib/fetchComponentData');

var config = require('./webpack.dev');
var path = require('path');
var express = require('express');

const app = express();

var compiler = webpack(config);


app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath,
}));

app.use(require('webpack-hot-middleware')(compiler));

// console.error(process.env.NODE_ENV)
// if (process.env.NODE_ENV !== 'production') {
//   require('./webpack.dev').default(app);
// }

app.use(express.static(path.join(__dirname, 'dist')));

app.use((req, res) => {
  const store = configureStore();
  const location = createLocation(req.url);


  reduxServer.match({ routes, location }, (err, redirectLocation, renderProps) => {
    if (err) {
      console.error(err);
      return res.status(500).end('Internal server error');
    }

    if (!renderProps) {
      return res.status(404).end('Not found');
    }

    function renderView() {
      const InitialView = (`
        <reactRedux.Provider store={store}>
          <reduxRouter.ReduxRouter />
        </reactRedux.Provider>`
      );

      const componentHTML = reactServer.renderToString(InitialView);

      const initialState = store.getState();

      const HTML = `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset='utf-8'>
          <title>Redux Demo</title>

          <script>
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
          </script>
          <link rel="stylesheet" type="text/css" href="/style.css" />
        </head>
        <body>
          <div id='react-view'>${componentHTML}</div>
          <script type='application/javascript' src='/bundle.js'></script>
      </html>
        </body>
      `;

      return HTML;
    }

    fetchComponentData(store.dispatch, renderProps.components, renderProps.params)
      .then(renderView)
      .then(html => res.end(html))
      .catch(_err => res.end(_err.message));
    return null;
  });
});

app.listen(3000, 'localhost', error => {
  if (error) {
    console.log(error);
    return;
  }

  console.log('Listening at http://localhost:3000');
});