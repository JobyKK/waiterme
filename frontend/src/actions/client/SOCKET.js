const LISTENER_MAP = {};

export const SOCKET = new WebSocket(`ws://${location.hostname}:${5000}/ws`);

SOCKET.emit = (code, data = '') => {
  SOCKET.send(JSON.stringify({
    code,
    data,
  }));
};

SOCKET.onopen = () => {
  SOCKET.emit('open');
  console.log('WS:OPEN');
};

SOCKET.onmessage = (message) => {
  if (message.data) {
    try {
      const data = JSON.parse(message.data);
      const code = data.code;
      if (code) {
        LISTENER_MAP[code](data.data);
      } else {
        console.log('WS:MESSAGE_WITHOUT_CODE', data);
      }
    } catch (e) {
      console.log('WS:MESSAGE_PARSE_ERROR', message.data);
      console.error(e);
    }
  } else {
    console.log('WS:MESSAGE_WITHOUT_DATA');
  }
};

SOCKET.on = (messageCode, func) => {
  LISTENER_MAP[messageCode] = func;
};

SOCKET.on('ANSWER', (data) => console.log('WS:ANSWER', data));
SOCKET.on('ERROR', (data) => console.error('WS_BACKEND:ERROR', data.error));
