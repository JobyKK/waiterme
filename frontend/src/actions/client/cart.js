import { SOCKET } from './SOCKET';

export const addToCart = (count, product) => {
  product.count = count;
  return {
    type: 'ADD_TO_CART',
    product,
  };
};

export const changeCart = (count, product) => {
  product.count = count;
  return {
    type: 'CHANGE_CART',
    product,
  };
};

export const sendOrder = (order, dispatch) => {
  SOCKET.emit('SEND_ORDER', {
    order,
  });

  SOCKET.on('SEND_ORDER_SUCCESS', (data) => {
    console.log('SEND_ORDER_SUCCESS', data);
    dispatch({
      type: 'SEND_ORDER_SUCCESS',
    });
  });

  return {
    type: 'SEND_ORDER',
  };
};
