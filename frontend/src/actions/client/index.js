export function verifyClientRequest(code) {
  return (dispatch) => {
    // todo remove mock
    // setTimeout(() => {
    //     dispatch(requestWaiterEvent())
    // }, 5000);
    return dispatch({ type: 'VERIFY_CLIENT', code });
  };

}

export function requestWaiterEvent() {
  return {
    type: 'REQUEST_WAITER',
  };
}

export function cancelWaiterRequest() {
  return {
    type: 'CANCEL_WAITER_REQUEST',
  };
}

export function cancelVerifyClientRequest() {
  return {
    type: 'CANCEL_VERIFY_CLIENT',
  };
}
