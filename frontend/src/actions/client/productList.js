export const select = (index) => ({
  type: 'SELECT',
  selectedIndex: index,
});

export const setCount = (count) => ({
  type: 'SET_COUNT',
  count,
});
