export function loginClient() {
  return {
    type: 'LOGIN_CLIENT',
  };
  // (dispatch) => {
  //     // todo remove mock
  //     // setTimeout(() => {
  //     //     dispatch(requestWaiterEvent())
  //     // }, 2000);
  //     return dispatch({ type: 'VERIFY_CLIENT', code });
  // };
}

export function loginManager() {
  return {
    type: 'LOGIN_MANAGER',
  };
}

export function loginWaiter() {
  return {
    type: 'LOGIN_WAITER',
  };
}