export const clickAddPersonal = (role) =>
  ({ type: 'MANAGER_LIST_SAVE_FIELD', field: { isRenderAddOverlay: true, role, name: '' } });
export const clickDisableAddOverlay = () =>
  ({ type: 'MANAGER_LIST_SAVE_FIELD', field: { isRenderAddOverlay: false } });
export const changePersonalName = (value) =>
  ({ type: 'MANAGER_LIST_SAVE_FIELD', field: { name: value } });
export const changePersonalRole = (value) =>
  ({ type: 'MANAGER_LIST_SAVE_FIELD', field: { role: value } });
export const addPersonal = () => ({ type: 'ADD_PERSONAL' });