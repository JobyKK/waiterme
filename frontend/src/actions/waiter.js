export const openEventPopup = (event) =>
  ({ type: 'WAITER_SAVE_FIELD', field: { isRenderOverlay: true, event } });
export const closeEventOverlay = () =>
  ({ type: 'WAITER_SAVE_FIELD', field: { isRenderOverlay: false, event: {} } });
export const setPopupContent = (value) =>
  ({ type: 'WAITER_SAVE_FIELD', field: { popupContent: value } });
export const changeEventStatus = (status) =>
  ({ type: 'CHANGE_EVENT_STATUS', status });
