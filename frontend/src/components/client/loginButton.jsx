import React from 'react';
import autobind from 'autobind-decorator'
import { addEvent, saveEventCreatorName } from 'actions/client';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

@autobind
class LoginButton extends React.Component {

    handleClickLoginEvent(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch(addEvent(this.props.eventCreatorName));
    }

    render() {
        return (
            <div>
                <button onClick={this.handleClickLoginEvent}>
                    add Event
                </button>
            </div>
        );
    }
}

LoginButton.propTypes = propTypes;
export default LoginButton;
