import React from 'react';
import autobind from 'autobind-decorator';
import { List, ListItem, makeSelectable } from 'material-ui/List';
import { select, setCount } from 'actions/client/productList';
import { addToCart } from 'actions/client/cart';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import FlatButton from 'material-ui/FlatButton';

import CSSTransition from 'react-transition-group/CSSTransition';

const SelectableList = makeSelectable(List);

const propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  selectedIndex: React.PropTypes.number,
  count: React.PropTypes.number,
  products: React.PropTypes.array,
  cartItems: React.PropTypes.array,
};

@autobind
export class ProductList extends React.Component {

  @autobind
  renderBuyBlock(isIn) {
    // TODO componentOnMount setCount(1);
    const { dispatch } = this.props;
    return (
      <CSSTransition
        in={isIn}
        timeout={{
          enter:500,
          exit: 500
        }}
        mountOnEnter
        unmountOnExit
        onEnter={(e) => dispatch(setCount(1))}
        classNames="fade"
      >
        <span
          style={{display: 'inline-block'}}
          onClick={(e) => e.stopPropagation()}
        >
          <FloatingActionButton
            mini
            onClick={(e) => dispatch(setCount(this.props.count - 1 || 1))}
          >-</FloatingActionButton>
          &nbsp;&nbsp;&nbsp;
          <input
            type="text"
            style={{'verticalAlign': '100%', width: '15px'}}
            value={this.props.count}
            onChange={(e) => {
              dispatch(setCount(e.target.value))
            }}
          />
          &nbsp;&nbsp;&nbsp;
          <FloatingActionButton
            mini
            onClick={(e) => dispatch(setCount(this.props.count + 1))}
          >+</FloatingActionButton>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <FlatButton
            label="Купить"
            backgroundColor="#a4c639"
            hoverColor="#8AA62F"
            style={{'verticalAlign': '100%'}}
            onClick={(e) => dispatch(
              addToCart(this.props.count, this.props.products[this.props.selectedIndex]))}
          />
        </span>
      </CSSTransition>
    );
  }

  render() {
    const { dispatch } = this.props;
    return (
      <div>
        <SelectableList>
          {this.props.products.map((item, index) =>
            <ListItem
              key={index}
              value={item.id}
              onClick={(e) => dispatch(
                select(this.props.selectedIndex === index ? -1 : index))
              }
              rightAvatar={
                this.renderBuyBlock(this.props.selectedIndex === index)
              }
            >
              <div>{item.name}</div>
            </ListItem>
          )}
        </SelectableList>
      </div>
    );
  }
}

ProductList.propTypes = propTypes;
