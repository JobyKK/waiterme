import React from 'react';
import autobind from 'autobind-decorator'
import { addEvent, saveEventCreatorName } from 'actions/waiter';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

@autobind
class Events extends React.Component {

    handleClickAddEvent(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch(addEvent(this.props.eventCreatorName));
    }

    handleInputAddEvent(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch(saveEventCreatorName(e.target.value));
    }

    render() {
        return (
            <div>
                <input
                    value={this.props.eventCreatorName}
                    onChange={this.handleInputAddEvent}
                />
                <button
                    onClick={this.handleClickAddEvent}
                >
                    add Event
                </button>
                <table>
                    <thead>
                    <tr>
                        <td>id</td>
                        <td>name</td>
                        <td>time start</td>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.getMeEvents.map((item) => (
                        <tr>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.timeStart}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

Events.propTypes = propTypes;
export default Events;
