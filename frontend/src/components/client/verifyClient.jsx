import React from 'react';
import autobind from 'autobind-decorator'
import {verifyClientRequest, cancelVerifyClientRequest} from 'actions/client';
import WaitingPanel from './waitingPanel'
import styles from './css/styles.css'

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
    code: React.PropTypes.string,
    isVerifyStart: React.PropTypes.bool,
};

@autobind
class VerifyClient extends React.Component {

    handleClickVerifyClientEvent(e) {
        e.preventDefault();
        const {dispatch} = this.props;
        console.log("verify client");
        dispatch(verifyClientRequest(e.target.value));
    }

    render() {
        return (
            <div>
                <div>
                    <span>Verify Client</span>
                </div>
                {this.props.isWaiterRequested ?
                    <WaitingPanel {...this.props} />
                    :
                    <div className={styles.test}>
                        <input
                            type='text'
                            value={this.props.code}
                        />
                        <button
                            onClick={this.handleClickVerifyClientEvent}>
                            Request Waiter
                        </button>
                    </div>

                }
            </div>
        );
    }
}

VerifyClient.propTypes = propTypes;
export default VerifyClient;
