import React from 'react';
import autobind from 'autobind-decorator'
import { cancelWaiterRequest } from '../../actions/client/index';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

@autobind
class WaitingPanel extends React.Component {

    handleClickCancelEvent(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch(cancelWaiterRequest(this.props.eventCreatorName));
    }

    render() {
        return (
            <div>
                <div>
                    <span>Please wait for a waiter</span>
                </div>
                <button
                    onClick={this.handleClickCancelEvent}
                >
                    Cancel
                </button>
            </div>
        );
    }
}

WaitingPanel.propTypes = propTypes;
export default WaitingPanel;