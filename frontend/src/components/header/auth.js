import React from 'react';
import style from './css/styles.css';

const propTypes = {
    userName: React.PropTypes.string,
};

class Auth extends React.Component {

  render() {
    return (
    <div id={style.info_button}>
      <button>
          {this.props.userName}
      </button>
    </div>
    );
  }
}

Auth.propTypes = propTypes;
export default Auth;
