import React from 'react';
import Menu from './menu';
import AuthInfo from './auth';

import styles from './css/styles.css';

const propTypes = {};

class Header extends React.Component {
    render() {
        return (
            <div className={styles.header}>
                {/*<span>*/}
                {/*<AuthInfo {...this.props} />*/}
                {/*</span>*/}
                <span>
                  <Menu {...this.props} />
                </span>
            </div>
        );
    }
}

Header.propTypes = propTypes;
export default Header;
