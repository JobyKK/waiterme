import React from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { isEmpty } from 'lodash';

import {
  AppBar,
  Divider,
  Drawer,
  FlatButton,
  IconButton,
  MenuItem
} from 'material-ui';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import ShoppingCartIcon from 'material-ui/svg-icons/action/shopping-cart';

import { toggleDrawer } from '../../actions/header';
import styles from './css/styles.css';

const propTypes = {
  drawer: React.PropTypes.object,
  cartItems: React.PropTypes.array,
  userType: React.PropTypes.string,
};

@autobind
class Menu extends React.Component {

  handleClickCartClientEvent() {
    this.props.dispatch(push({ pathname: '/client/cart' }));
  }

  handleClickLoginClientEvent() {
    this.props.dispatch(push({ pathname: '/' }));
  };

  handleDrawerToggle() {
    this.props.dispatch(toggleDrawer());
  }

  renderActionIcons() {
    return (
      <div>
        {this.props.userType === 'CLIENT' ?
          <IconButton
            onClick={this.handleClickCartClientEvent}
          >
            <ShoppingCartIcon color={isEmpty(this.props.cartItems) ? "black" : "red"} />
          </IconButton>
          :
          null
        }
        <FlatButton
          className={styles.login_button}
          onClick={this.handleClickLoginClientEvent}
          label="Login"
        />
      </div>
    );
  }

  render() {
    return (
      <div id={styles.menu_button}>
        <AppBar
          className={styles.header_content}
          title="Waiterme"
          onLeftIconButtonTouchTap={this.handleDrawerToggle}
          iconElementRight={this.renderActionIcons()}
        >

        </AppBar>
        <Drawer
          open={this.props.drawer.open}
          docked={false}
          onRequestChange={this.handleDrawerToggle}
        >
          <MenuItem
            primaryText="Новости"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Меню"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Зказ"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Соц сети"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Считать QR-код"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Вызов официанта"
            leftIcon={<RemoveRedEye/>}
          />

          <Divider/>

          <MenuItem
            primaryText="Уведомнеия"
            leftIcon={<RemoveRedEye/>}
          />
          <MenuItem
            primaryText="Настройки"
            leftIcon={<RemoveRedEye/>}
          />
        </Drawer>
      </div>
    );
  }
}

Menu.propTypes = propTypes;

function mapStateToProps(state) {
  const { drawer } = state;
  return {
    drawer
  };
}

export default connect(mapStateToProps)(Menu);