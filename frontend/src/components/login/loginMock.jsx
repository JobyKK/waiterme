import React from 'react';
import autobind from 'autobind-decorator'
import {IconMenu, IconButton, MenuItem, AppBar, FlatButton, Drawer, Divider} from 'material-ui';
import { loginClient, loginManager, loginWaiter } from "actions/login/login";
import  { push } from 'redux-router';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

@autobind
class LoginMock extends React.Component {

    handleClickLoginClientEvent() {
        console.log('client');
        const {dispatch} = this.props;
        dispatch(push({ pathname: '/client' }));
        dispatch(loginClient());
    }
    handleClickLoginWaiterEvent() {
        console.log('waiter');
        const {dispatch} = this.props;
        dispatch(push({ pathname: '/waiter' }));
        dispatch(loginWaiter());
    }
    handleClickLoginManagerEvent() {
        console.log('manager');
        const {dispatch} = this.props;
        dispatch(push({ pathname: '/manager' }));
        dispatch(loginManager());
    }

    render() {
        return (
            <div>
                <FlatButton
                    label="Login"
                />
                <button
                    onClick={this.handleClickLoginClientEvent}>
                    Login Client
                </button>
                <button
                    onClick={this.handleClickLoginWaiterEvent}>
                    Login Waiter
                </button>
                <button
                    onClick={this.handleClickLoginManagerEvent}>
                    Login Manager
                </button>
            </div>
        );
    }
}

LoginMock.propTypes = propTypes;
export default LoginMock;