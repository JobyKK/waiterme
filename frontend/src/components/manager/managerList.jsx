import React from 'react';
import autobind from 'autobind-decorator';
import { addPersonal, changePersonalName, changePersonalRole, clickDisableAddOverlay, clickAddPersonal }
  from 'actions/manager/managerList'
import { ROverlay } from 'components/r';
import l from 'lodash';
import { ROLES } from 'lib/utils/manager';

const propTypes = {
  personal: React.PropTypes.object,
  isRenderAddOverlay: React.PropTypes.bool,
};

@autobind
class ManagerList extends React.Component {

  renderAddButton(text, role) {
    const { dispatch : d } = this.props;
    return (
        <button
          onClick={() => d(clickAddPersonal(role))}
        >{text}
        </button>
    )
  }

  renderPopupContent() {
    const { dispatch : d } = this.props;
    return (
      <div>
        <input
          type='text'
          onChange={(e) => d(changePersonalName(e.target.value))}
          value={this.props.name}
        />
        <br />
        <select
          onChange={(e) => d(changePersonalRole(e.target.value))}
          value={this.props.role}
        >
          <option value='manager'>manager</option>
          <option value='admin'>admin</option>
          <option value='waiter'>waiter</option>
        </select>
        <button
          style={{margin: '60% auto auto 75% '}}
          onClick={() => {
            d(addPersonal());
            d(clickDisableAddOverlay())
          }}
        >
          Add
        </button>
      </div>
    )
  }

  renderWaiters() {
    return (
      <div>
        <div>Waiters:</div>
        <div style={{paddingLeft: '20px'}}>
          {Object.values(l.filter(this.props.personal, (e) => e.role == ROLES.waiter)).map((person) => (
            <div>
              <span>
                {`${person.id} `}
              </span>
              <span>
                {person.name}
              </span>
            </div>
          ))}
        {this.renderAddButton('add Waiter', 'waiter')}
        </div>
      </div>
    )
  }
  
  renderManagers() {
    return (
      <div>
        <div>Managers:</div>
        <div style={{paddingLeft: '20px'}}>
          {Object.values(l.filter(this.props.personal, (e) => e.role != ROLES.waiter)).map((person) => (
            <div>
              <span>
                {`${person.id} `}
              </span>
              <span>
                {person.name}
              </span>
              <span>
                {person.role}
              </span>
            </div>
          ))}
        {this.renderAddButton('add Manager', 'manager')}
        </div>
      </div>
    )
  }

  render() {
    const { dispatch : d } = this.props;
    return (
      <div>
        <ROverlay
          visible={this.props.isRenderAddOverlay}
          onClose={() => d(clickDisableAddOverlay())}
          content={this.renderPopupContent()}
        />
        {this.renderWaiters()}
        {this.renderManagers()}
      </div>
    );
  }
}

ManagerList.propTypes = propTypes;
export default ManagerList;
