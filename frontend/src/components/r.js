import React from 'react';

const propTypes = {
  visible: React.PropTypes.bool,
  content: React.PropTypes.any,
  onClose: React.PropTypes.func,
};

class ROverlay extends React.Component {

  render() {
    return (
      <div
        style={{
          backgroundColor: 'rgba(0,0,0,0.5)',
          width: this.props.visible ? '100%' : '0px',
          height: this.props.visible ? '3000px' : '0px',
          overflow: 'hidden',
          position: 'fixed',
          top: '0px',
        }}
      >
        <div
          style={{
            margin: '50px auto 0px auto',
            width: '50%',
            padding: '10px',
            backgroundColor: '#c5c5c5',
            borderRadius: '5px',
            boxShadow: '0px 0px 10px #000',
          }}
        >
          <button
            style={{
              marginLeft: '95%',
            }}
            onClick={this.props.onClose}
          >X</button>
          <div>
            {this.props.content}
          </div>
        </div>
      </div>
    );
  }
}

ROverlay.propTypes = propTypes;
export { ROverlay };
