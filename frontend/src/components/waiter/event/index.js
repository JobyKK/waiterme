import React from 'react';
import { formatDate } from 'lib/utils/waiter';

const propTypes = {
  event: React.PropTypes.object,
  onClick: React.PropTypes.func,
  isRenderStatus: React.PropTypes.bool,
};

class Event extends React.Component {

  render() {
    return (
      <tr onClick={this.props.onClick}>
        <td>
          {this.props.event.type}
        </td>
        <td>
          {this.props.event.table.number}
        </td>
        <td>
          {this.props.event.user.name}
        </td>
        <td>
          {formatDate(this.props.event.dateCreated)}
        </td>
        {this.props.isRenderStatus ?
          <td>
            {this.props.event.status}
          </td>
          :
          null
        }
      </tr>
    );
  }
}

Event.propTypes = propTypes;

export default Event;
