import React from 'react';
import autobind from 'autobind-decorator'
import { ROverlay } from 'components/r';
import { closeEventOverlay, openEventPopup, setPopupContent, changeEventStatus } from 'actions/waiter';
import styles from './waiter.css';
import l from 'lodash';
import Event from './event/index';
import { WAITER_EVENT_STATUS } from 'lib/utils/waiter';

const propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  events: React.PropTypes.array,
  event: React.PropTypes.object,
  isRenderOverlay: React.PropTypes.bool,
  popupContent: React.PropTypes.string,
};


@autobind
class EventsList extends React.Component {

  renderMap() {
    return(
    <div className={styles.map}>
      Карта
    </div>
    );
  }

  renderMenu() {
    return(
      <div className={styles.menu}>
        Меню
      </div>
    );
  }

  renderMainPopupContent() {
    const { dispatch : d } = this.props;
    return (
      <div>
        <div className={styles.popupContent}>
          <div className={styles.menuContent}>
            <button onClick={()=>d(setPopupContent('map'))}>
              Карта
            </button>
            <br />
            <button onClick={()=>d(setPopupContent('menu'))}>
              Меню
            </button>
          </div>
          <div className={styles.content}>
            {this.props.popupContent == 'map'?
              this.renderMap()
              :
              this.renderMenu()
            }
          </div>
        </div>
      </div>
    )
  }

  renderPopupContent() {
    const { dispatch : d, event } = this.props;
    return !l.isEmpty(event) && (
      <div>
        <table className={styles.eventHeader}>
          <thead>
            <tr>
              <th>Тип</th>
              <th>N столика</th>
              <th>Имя</th>
              <th>Время заказа</th>
              <th>Статус</th>
            </tr>
          </thead>
          <tbody>
            <Event event={event} isRenderStatus={true}/>
          </tbody>
        </table>
        <hr />
        {this.renderMainPopupContent()}
        <div className={styles.footer}>
          {event.type == WAITER_EVENT_STATUS.NEW ?
            null
            :
            <button
              onClick={() => d(changeEventStatus(WAITER_EVENT_STATUS.NEW))}>
              Отказаться от события
            </button>
          }
          <button
            onClick={() => d(changeEventStatus(event.type == WAITER_EVENT_STATUS.IN_PROGRESS ?
              WAITER_EVENT_STATUS.CLOSED:
              WAITER_EVENT_STATUS.IN_PROGRESS))}>
            {event.type == WAITER_EVENT_STATUS.IN_PROGRESS ? 'Подтвердить': 'Взять событие'}
          </button>
          <button onClick={() => d(closeEventOverlay())}>
            Назад
          </button>
        </div>
      </div>
    )
  }

  renderEventList() {
    const { dispatch : d } = this.props;
    return (
      <table className={styles.eventTable}>
        <thead>
        <tr>
          <th>Тип</th>
          <th>N столика</th>
          <th>Имя</th>
          <th>Время заказа</th>
        </tr>
        </thead>
        <tbody>
          {this.props.events.map((event) => (
            <Event
              onClick={() => {d(openEventPopup(event))}}
              event={event}/>
          ))}
        </tbody>
      </table>
    )
  }

  render() {
    const { dispatch : d } = this.props;
    return (
      <div>
        <ROverlay
          visible={this.props.isRenderOverlay}
          onClose={() => d(closeEventOverlay())}
          content={this.renderPopupContent(event)}
        />
        {this.renderEventList()}
      </div>
    );
  }
}


EventsList.propTypes = propTypes;
export default EventsList;
