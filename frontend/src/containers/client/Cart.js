import React from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { changeCart, sendOrder } from 'actions/client/cart';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { FlatButton } from 'material-ui';
import { isEmpty } from 'lodash';

const propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  items: React.PropTypes.array,
  orderStatus: React.PropTypes.string,
};

@autobind
class CartContainer extends React.Component {



  renderItems() {
    const { dispatch }  = this.props;
    return (
      <div>
        {this.props.items.map((product, i) => (
          <div key={i}>
            {`${product.name} : ${product.count} шт`}
            <FloatingActionButton
              mini
              onClick={(e) => dispatch(changeCart(product.count - 1, product))}
            >-</FloatingActionButton>
            <FloatingActionButton
              mini
              onClick={(e) => dispatch(changeCart(product.count + 1, product))}
            >+</FloatingActionButton>
          </div>
        ))}
        <FlatButton
          label="Заказать"
          backgroundColor="green"
          onClick={(e) => dispatch(sendOrder(this.props.items, dispatch))}
        />
      </div>
    );
  }

  renderEmptyText() {
    switch (this.props.orderStatus) {
      case 'new':
        return <div>Ваша корзина пуста</div>;
      case 'sent':
        return <div>Ожидайте, ваш заказ отправлен</div>;
      case 'inProgress':
        return <div>Ожидайте, ваш заказ в обработке</div>;
    }
  }

  render() {
    return (
      <div>
        {isEmpty(this.props.items) ?
          this.renderEmptyText()
          :
          this.renderItems()
        }
        {this.props.children}
      </div>
    );
  }
}

CartContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return state.client.cart;
}

export default connect(mapStateToProps)(CartContainer);
