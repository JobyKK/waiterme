import React from 'react';
import { connect } from 'react-redux';
import VerifiedClient from './VerifiedClient';
import VerifyHelp from './VerifyHelp';

const propTypes = {
  getMeEvents: React.PropTypes.array,
  dispatch: React.PropTypes.func.isRequired,
  isVerified: React.PropTypes.bool,
};

class ClientContainer extends React.Component {

  render() {
    return (
      <div>
        <VerifiedClient {...this.props} />
        {/*{this.props.isVerified ?*/}
        {/*<VerifyHelp {...this.props}/>*/}
        {/*}*/}
        {this.props.children}
      </div>
    );
  }
}

ClientContainer.propTypes = propTypes;

function mapStateToProps(state) {
  const client = state.client.main;
  return {
    getMeEvents: client.getMeEvents,
    eventCreatorName: client.eventCreatorName,
    isVerified: client.isVerified,
    isWaiterRequested: client.isWaiterRequested,
  };
}

export default connect(mapStateToProps)(ClientContainer);
