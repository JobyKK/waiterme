import React from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { FlatButton } from 'material-ui';

const propTypes = {
  dispatch: React.PropTypes.func.isRequired,
};

@autobind
class ClientMenuContainer extends React.Component {

  render() {
    return (
      <div>
        {window.location.pathname === '/client/menu' ?
          <FlatButton
            label="TEST menu items"
            onClick={(e) => this.props.dispatch(push({ pathname: '/client/menu/test' }))}
          />
          :
          this.props.children
        }
      </div>
    );
  }
}

ClientMenuContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return state.client.menu;
}

export default connect(mapStateToProps)(ClientMenuContainer);
