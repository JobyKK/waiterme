import React from 'react';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import { ProductList } from 'components/client/productList';

const propTypes = {
  dispatch: React.PropTypes.func.isRequired,
};

@autobind
class ClientMenuContainer extends React.Component {
  render() {
    return (
      <ProductList {...this.props} />
    );
  }
}

ClientMenuContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return Object.assign({}, state.client.productList, {
    cartItems: state.client.cart.items
  });
}

export default connect(mapStateToProps)(ClientMenuContainer);
