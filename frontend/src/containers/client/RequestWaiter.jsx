import React from 'react';
import autobind from 'autobind-decorator';
// import {addEvent, saveEventCreatorName} from 'actions/client/index';
import { requestWaiterEvent } from '../../actions/client/index';
import WaitingPanel from './../../components/client/waitingPanel';
import { connect } from 'react-redux';

const propTypes = {
  getMeEvents: React.PropTypes.array,
  dispatch: React.PropTypes.func.isRequired,
  isWaiterRequested: React.PropTypes.bool
};

@autobind
class RequestWaiterContainer extends React.Component {

  handleClickRequestWaiterEvent(e) {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(requestWaiterEvent());
  }

  render() {
    return (
      <div>
        <div>
          <span>Request Waiter</span>
          <div>{this.props.isWaiterRequested}</div>
        </div>
        {this.props.isWaiterRequested ?
          <WaitingPanel {...this.props} />
          :
          <button
            onClick={this.handleClickRequestWaiterEvent}>
            Request Waiter
          </button>
        }
      </div>
    );
  }
}

RequestWaiterContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return state.client.requestWaiter;
}

export default connect(mapStateToProps)(RequestWaiterContainer);
