import React from 'react';
import autobind from 'autobind-decorator';
import { FlatButton } from 'material-ui';
import { push } from 'redux-router';

const propTypes = {
  dispatch: React.PropTypes.func.isRequired
};

@autobind
class VerifiedClientPage extends React.Component {

  handleClickRequestWaiterEvent(e) {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(e());
  }

  handleMenuClickedEvent() {
    const { dispatch } = this.props;
    dispatch(push({ pathname: '/client/menu' }));
  }

  handleRequestWaiterClickEvent() {
    const { dispatch } = this.props;
    dispatch(push({ pathname: '/client/waiter' }));
  }

  render() {
    return (
      <div>
        <FlatButton label="Menu" onClick={this.handleMenuClickedEvent}/>
        <FlatButton
          label="Request waiter"
          onClick={this.handleRequestWaiterClickEvent}
        />
      </div>
    );
  }
}

VerifiedClientPage.propTypes = propTypes;
export default VerifiedClientPage;