import React from "react";
import autobind from "autobind-decorator";
import {FlatButton} from "material-ui";
import {browserHistory} from "react-router";

const propTypes = {
    dispatch: React.PropTypes.func.isRequired
};

@autobind
class VerifyHelpPage extends React.Component {

    render() {
        return (
            <div>
                Here should be guide how to verify yourself in a restaurant
            </div>
        );
    }
}

VerifyHelpPage.propTypes = propTypes;
export default VerifyHelpPage;