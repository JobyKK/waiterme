import React from 'react';
import { connect } from 'react-redux';
import RegistrationEvents from '../../components/client/registration';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

class ClientSeatRegistrationContainer extends React.Component {

    render() {
        return (
            <div>
                <RegistrationEvents {...this.props} />
            </div>
        );
    }
}

ClientSeatRegistrationContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const { client } = state;
    return {
        getMeEvents: client.getMeEvents,
        eventCreatorName: client.eventCreatorName,
    };
}

export default connect(mapStateToProps)(ClientSeatRegistrationContainer);
