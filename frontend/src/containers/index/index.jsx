import React from 'react';
import Header from 'components/header';
import { connect } from 'react-redux';
import styles from './css/styles.css';

const propTypes = {
  children: React.PropTypes.object,
};

class App extends React.Component {

  render() {
    return (
      <div id="main-view" className={styles.container}>
        <Header {...this.props} />
        <div className={styles.content}>
             {this.props.children}
        </div>
      </div>
    );
  }
}

App.propTypes = propTypes;


function mapStateToProps(state) {
  return {
    cartItems: state.client.cart.items,
    userType: state.user.userType,
  };
}

export default connect(mapStateToProps)(App);
