import React from 'react';
import { connect } from 'react-redux';
import LoginMock from '../../components/login/loginMock';

const propTypes = {
    getMeEvents: React.PropTypes.array,
    dispatch: React.PropTypes.func.isRequired,
};

class LoginContainer extends React.Component {

    render() {
        return (
            <div>
                <LoginMock {...this.props} />
            </div>
        );
    }
}

LoginContainer.propTypes = propTypes;

function mapStateToProps(state) {
    const { user } = state;
    return {
        eventCreatorName: user.eventCreatorName,
    };
}

export default connect(mapStateToProps)(LoginContainer);
