import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { FlatButton } from 'material-ui';

const propTypes = {
  getMeEvents: React.PropTypes.array,
  dispatch: React.PropTypes.func.isRequired,
  isVerified: React.PropTypes.bool,
};

class ManagerContainer extends React.Component {

  render() {
    return (
      <div>
        <Link to={{ pathname: '/manager/list' }}>
          <FlatButton label="Manager list" />
        </Link>
        <Link to={{ pathname: '/manager/menu' }}>
          <FlatButton label="Menu"/>
        </Link>
        <Link to={{ pathname: '/manager/construct' }}>
          <FlatButton label="Construct" />
        </Link>
        {this.props.children}
      </div>
    );
  }
}

ManagerContainer.propTypes = propTypes;

function mapStateToProps(state) {
  const client = state.client.main;
  return {
    getMeEvents: client.getMeEvents,
    eventCreatorName: client.eventCreatorName,
    isVerified: client.isVerified,
    isWaiterRequested: client.isWaiterRequested,
  };
}

export default connect(mapStateToProps)(ManagerContainer);
