import React from 'react';
import { connect } from 'react-redux';
import ManagerList from 'components/manager/managerList';

const propTypes = {};

class ManagerListContainer extends React.Component {

  render() {
    return (
      <div>
        <ManagerList {...this.props} />
      </div>
    );
  }
}

ManagerListContainer.propTypes = propTypes;

function mapStateToProps(state) {
  const { name, role, personal, isRenderAddOverlay } = state.manager.managerList;
  return {
    personal,
    isRenderAddOverlay,
    name,
    role,
  };
}

export default connect(mapStateToProps)(ManagerListContainer);
