import React from 'react';
import { connect } from 'react-redux';

const propTypes = {
  menuList: React.PropTypes.object,
};

class MenuListContainer extends React.Component {

  render() {
    return (
      <div>Menu List</div>
    );
  }
}

MenuListContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return state.manager.menuList;
}

export default connect(mapStateToProps)(MenuListContainer);
