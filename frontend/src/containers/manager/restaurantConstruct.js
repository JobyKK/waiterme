import React from 'react';
import { connect } from 'react-redux';

const propTypes = {
  halls: React.PropTypes.object,
};

class restaurantConstructContainer extends React.Component {

  render() {
    return (
      <div>
        <h1>restaurant Construct </h1>
        <pre>
          {this.props.halls}
        </pre>
      </div>
    );
  }
}

restaurantConstructContainer.propTypes = propTypes;

function mapStateToProps(state) {
  return {
    restaurantConstruct: state.manager.restaurantConstruct,
  };
}

export default connect(mapStateToProps)(restaurantConstructContainer);
