import React from 'react';
import { connect } from 'react-redux';
import EventList from '../../components/waiter/eventList';

const propTypes = {
};

class WaiterContainer extends React.Component {

  render() {
    return (
      <div>
        <EventList {...this.props} />
      </div>
    );
  }
}

WaiterContainer.propTypes = propTypes;

function mapStateToProps(state) {
  const { events, isRenderOverlay, event, popupContent } = state.waiter;
  return {
    events,
    isRenderOverlay,
    event: Object.assign({}, event, {
      type: event.type,
      table: event.table || {},
      user: event.user || {},
      status: event.status,
      dateCreated: event.dateCreated || new Date(),
    }),
    popupContent,
  };
}

export default connect(mapStateToProps)(WaiterContainer);
