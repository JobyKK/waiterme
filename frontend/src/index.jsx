import 'babel-polyfill';
// import 'fastclick';
// import 'isomorphic-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'model/index';
import { ReduxRouter } from 'redux-router';

import routes from 'routes';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// eslint-disable-next-line
const store = configureStore(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
// const store = configureStore(window.__INITIAL_STATE__);

injectTapEventPlugin();

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme()}>
      <ReduxRouter children={routes} />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('react-view')
);
