export const ROLES = {
  waiter: 1,
  manager: 2,
  admin: 3,
};

export const TABLE_STATUS = {
  free: 1,
  booked: 2,
  close: 3,
  used: 4,
};

export const HALL_STATUS = {
  open: 1,
  close: 2,
  booked: 3,
};

export const getId = () => Math.round(Math.random() * 1000);
