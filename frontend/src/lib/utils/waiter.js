export const WAITER_EVENT_TYPE = {
  VERIFY: 0,
  GET_ME: 1,
};

export const WAITER_EVENT_STATUS = {
  NEW: 'новое',
  OPEN: 'открыто',
  IN_PROGRESS: 'в процессе',
  CLOSED: 'закрыто',
};

export const formatDate = (date) =>
  date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
