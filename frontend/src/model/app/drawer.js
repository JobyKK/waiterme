export class Drawer {
    constructor(initialState) {
        this.open = false;
        Object.assign(this, initialState);
    }
}
