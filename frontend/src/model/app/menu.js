export class Menu {
    constructor(initialState) {
        this.drawer = {
            open: false
        };
        Object.assign(this, initialState);
    }
}
