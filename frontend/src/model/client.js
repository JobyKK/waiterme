export class Client {
  constructor(initialState) {
    this.getMeEvents = [];
    this.isVerified = false;
    this.code = '';
    Object.assign(this, initialState);
  }
}
