export class User {
  constructor(initialState) {
    // React.PropTypes.oneOf(['ANONYMOUS', 'CLIENT', 'MANAGER', 'WAITER'])
    this.userType = 'CLIENT';
    Object.assign(this, initialState);
  }
}
