export class ManagerList {
  constructor(initialState) {
    this.personal = {};
    this.isRenderAddOverlay = false;
    this.name = '';
    this.role = '';
    Object.assign(this, initialState);
  }
}