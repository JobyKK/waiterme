import { TABLE_STATUS, HALL_STATUS } from 'lib/utils/manager';

export class Table {
  constructor(initialState) {
    this.maxPerson = 0;
    this.status = TABLE_STATUS.free;
    Object.assign(this, initialState);
  }
}

export class Hall {
  constructor(initialState) {
    Object.assign(this, initialState);
    this.tables = {};
    this.status = HALL_STATUS.open;
  }
}

export class RestaurantConstruct {
  constructor(initialState) {
    this.halls = {};
    Object.assign(this, initialState);
  }
}
