export class ClientSeat {
    constructor(initialState) {
        Object.assign(this, initialState);
        this.getMeEvents = [];
    }
}