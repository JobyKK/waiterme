export class Waiter {
  constructor(initialState) {
    this.events = [];
    this.isRenderOverlay = false;
    this.event = {};
    this.popupContent = 'map'; // as default
    Object.assign(this, initialState);
  }
}
