import { Drawer } from '../../model/app/drawer';

export function drawer(state = new Drawer(), action) {
    switch (action.type) {
        case 'TOGGLE_DRAWER':
            return Object.assign({}, state, {
                open: !state.open,
            });
        default:
            return state;
    }
}
