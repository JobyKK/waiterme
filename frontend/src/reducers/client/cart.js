function addToCart(state, action) {
  const newCartItems = state.items.slice(0);
  let productIndex = -1;
  const product = newCartItems.find((item, i) => {
    if (item.id === action.product.id) {
      productIndex = i;
      return true;
    }
    return false;
  });

  if (productIndex !== -1) {
    newCartItems[productIndex] = Object.assign({}, newCartItems[productIndex], {
      count: product.count + action.product.count,
    });
  } else {
    newCartItems.push(action.product);
  }
  return Object.assign({}, state, { items: newCartItems });
}

function changeCart(state, action) {
  const newCartItems = state.items.slice(0);
  let productIndex = -1;
  newCartItems.find((item, i) => {
    if (item.id === action.product.id) {
      productIndex = i;
      return true;
    }
    return false;
  });

  if (action.product.count === 0) {
    newCartItems.splice(productIndex, 1);
  } else {
    newCartItems[productIndex] = Object.assign({}, newCartItems[productIndex], {
      count: action.product.count,
    });
  }

  return Object.assign({}, state, { items: newCartItems });
}

export function cartReducer(state = {
  items: [],
  orderStatus: 'new',
}, action) {
  switch (action.type) {
    case 'ADD_TO_CART':
      return addToCart(state, action);
    case 'CHANGE_CART':
      return changeCart(state, action);
    case 'SEND_ORDER':
      return Object.assign({}, state, { orderStatus: 'sent', items: [] });
    case 'SEND_ORDER_SUCCESS':
      return Object.assign({}, state, { orderStatus: 'inProgress' });
    default:
      return state;
  }
}
