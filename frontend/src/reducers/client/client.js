import { Client } from 'model/client';

export function clientReducer(state = new Client({ eventCreatorName: 'example name' }), action) {
  switch (action.type) {
    case 'REQUEST_WAITER':
      return Object.assign({}, state, {
        isWaiterRequested: true,
      });
    case 'CANCEL_WAITER_REQUEST':
      console.log('CANCEL_WAITER_REQUEST');
      return Object.assign({}, state, {
        isWaiterRequested: false,
      });
    case 'CANCEL_VERIFY_CLIENT':
      return Object.assign({}, state, {
        isWaiterRequested: false,
      });
    case 'VERIFY_CLIENT':
      return Object.assign({}, state, {
        code: action.code,
        isWaiterRequested: true,
        // isVerified: true
      });
    default:
      return state;
  }
}
