import { combineReducers } from 'redux';
import { clientReducer } from './client';
import { clientMenuReducer } from './menu';
import { requestWaiterReducer } from './requestWaiter';
import { productListReducer } from './productList';
import { cartReducer } from './cart';


const client = combineReducers({
  main: clientReducer,
  menu: clientMenuReducer,
  requestWaiter: requestWaiterReducer,
  productList: productListReducer,
  cart: cartReducer,
});

export default client;
