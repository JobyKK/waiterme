export class Product {
  constructor(name) {
    this.id = Math.trunc(Math.random() * 10000000);
    this.name = name;
  }
}

export function productListReducer(state = {
  count: 1,
  selectedIndex: -1,
  products: ['test1', 'test2', 'test3', 'test4'].map((name) =>
    new Product(name)),
}, action) {
  switch (action.type) {
    case 'SELECT':
      return Object.assign({}, state, { selectedIndex: action.selectedIndex });
    case 'SET_COUNT':
      return Object.assign({}, state, { count: action.count });
    default:
      return state;
  }
}
