import { ClientSeat } from 'model/client';

export function client(state = new ClientSeat({ eventCreatorName: 'clientSeatEvents' }), action) {
  switch (action.type) {
    case 'REGISTER_EVENT':
      return Object.assign({}, state, {
        getMeEvents: state.getMeEvents.concat([action.getMeEvent]),
      });
    case 'SAVE_FIELD':
      return Object.assign({}, state, action.field);
    default:
      return state;
  }
}