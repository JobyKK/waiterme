import { Menu } from 'model/app/menu';

export function headerMenuReducer(state = new Menu(), action) {
  switch (action.type) {
    case 'TOGGLE_DRAWER':
      return Object.assign({}, state, {
        isWaiterRequested: true,
      });
    default:
      return state;
  }
}
