import { combineReducers } from 'redux';
import manager from '../reducers/manager/index';
import { waiter } from '../reducers/waiter';
import client from '../reducers/client/index.js';
import { user } from '../reducers/login';
import { drawer } from '../reducers/app/drawer';
import { routerStateReducer } from 'redux-router';

const rootReducer = combineReducers({
  router: routerStateReducer,
  manager,
  waiter,
  client,
  user,
  drawer,
});

export default rootReducer;
