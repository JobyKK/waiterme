import { User } from 'model/login';
import { Client } from 'model/client';
import { Waiter } from 'model/waiter';

export function user(state = new User({ eventCreatorName: 'example name' }), action) {
  switch (action.type) {
    case 'LOGIN_CLIENT':
      return Object.assign({}, state, {
        userType: 'CLIENT',
        user: Client,
      });
    case 'LOGIN_WAITER':
      return Object.assign({}, state, {
        userType: 'WAITER',
        user: Waiter,
      });
    case 'LOGIN_MANAGER':
      return Object.assign({}, state, {
        userType: 'MANAGER',
      });
    default:
      return state;
  }
}
