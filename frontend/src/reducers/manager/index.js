import { combineReducers } from 'redux';
import { managerList } from './managerList';
import { menuList } from './menuList';
import { restaurantConstruct } from './restaurantConstruct';

const manager = combineReducers({
  managerList,
  menuList,
  restaurantConstruct,
});

export default manager;
