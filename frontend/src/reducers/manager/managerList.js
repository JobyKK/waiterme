import { ManagerList } from 'model/manager/managerList';
import { ROLES, getId } from 'lib/utils/manager';

const testWaiter = { id: getId(), name: 'vasa', role: ROLES.waiter };
const testManager = { id: getId(), name: 'peta', role: ROLES.manager };

const MOCK = new ManagerList();
MOCK.personal[testWaiter.id] = testWaiter;
MOCK.personal[testManager.id] = testManager;

const addPersonal = (state) => {
  const person = {
    name: state.name,
    id: Math.round(Math.random() * 10000),
    dateCreated: new Date(),
    role: ROLES[state.role],
  };
  state.personal[person.id] = person;
  return state;
};

export function managerList(state = new ManagerList(MOCK), action) {
  switch (action.type) {
    case 'ADD_PERSONAL':
      return addPersonal(state);
    case 'MANAGER_LIST_SAVE_FIELD':
      return Object.assign({}, state, action.field);
    default:
      return state;
  }
}
