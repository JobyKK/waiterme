import { RestaurantConstruct } from 'model/manager/restaurantConstruct';

const addHall = (state, hall) => {
  state.halls[hall.id] = hall;
  return state;
};

const addTable = (state, hallId, table) => {
  state.halls[hallId].tables[table.id] = table;
  return state;
};

export function restaurantConstruct(state = new RestaurantConstruct(), action) {
  switch (action.type) {
    case 'ADD_HALL':
      return addHall(state, action.hall);
    case 'ADD_TABLE':
      return addTable(state, action.hallId, state.table);
    case 'SAVE_FIELD':
      return Object.assign({}, state, action.field);
    default:
      return state;
  }
}
