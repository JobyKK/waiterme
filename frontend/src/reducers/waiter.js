import { Waiter } from 'model/waiter';
import { WAITER_EVENT_STATUS, WAITER_EVENT_TYPE } from 'lib/utils/waiter';

const initialState = {
  events: [
    {
      type: WAITER_EVENT_TYPE.VERIFY,
      table: { number: 10 },
      user: { name: 'vasa pupkin' },
      status: WAITER_EVENT_STATUS.NEW,
      dateCreated: new Date(),
    },
  ],
};

const changeEventStatus = (state, status) => {
  state.event.status = status;
  return Object.assign({}, state);
};

export function waiter(state = new Waiter(initialState), action) {
  switch (action.type) {
    case 'WAITER_SAVE_FIELD':
      return Object.assign({}, state, action.field);
    case 'CHANGE_EVENT_STATUS':
      return changeEventStatus(state, action.status);
    default:
      return state;
  }
}
