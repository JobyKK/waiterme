import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from 'containers/index/index';
import Manager from 'containers/manager';
import ManagerList from 'containers/manager/managerList';
import MenuList from 'containers/manager/menuList';
import RestaurantConstruct from 'containers/manager/restaurantConstruct';
import Waiter from 'containers/waiter/waiter';
import Client from 'containers/client/index';
import LoginContainer from 'containers/login/loginManager';
import ClientMenu from 'containers/client/ClientMenu';
import ProductList from 'containers/client/ProductList';
import Cart from 'containers/client/Cart';

import RequestWaiter from 'containers/client/RequestWaiter';

export default (
  <Route path="/" component={App} name="app">
    <IndexRoute component={LoginContainer} />
    <Route path="manager" component={Manager} name="manager">
      <Route path="list" component={ManagerList} name="managerList" />
      <Route path="menu" component={MenuList} name="menuList" />
      <Route path="construct" component={RestaurantConstruct} name="restaurantConstruct" />
    </Route>
    <Route path="waiter" component={Waiter} name="waiter" />
    <Route path="client" component={Client} name="client">
      <Route path="menu" component={ClientMenu} name="clientMenu">
        <Route path=":productGroup" component={ProductList} name="productList" />
      </Route>
      <Route path="cart" component={Cart} name="cart" />
      <Route path="waiter" component={RequestWaiter} name="requestWaiter" />
    </Route>
  </Route>
);
